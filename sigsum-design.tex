\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{nopageno}
\usepackage{hyperref}
\usepackage{url}
\usepackage{color}
\usepackage{tikz}
\usepackage{amsmath}

\definecolor{darkblue}{RGB}{0,0,139}
\hypersetup{
  colorlinks,
  citecolor=darkblue,
  linkcolor=darkblue,
  urlcolor=darkblue,
}

\title{Sigsum\\A minimal design for public transparency logs}
\date{November 30, 2023}

\begin{document}
\maketitle
\begin{tikzpicture}[remember picture,overlay]
	\node[anchor=north west,inner sep=10pt] at (current page.north west)
	{\parbox{14cm}{%
		Supplementary notes accompanying the CATS 2023 lightning talk by Niels Möller, see:\\
		\url{https://catsworkshop.dev/program/}%
	}};
\end{tikzpicture}


\begin{abstract}

  These notes describe and motivate the design of the Sigsum transparency log
  system. We cover the challenges and design alternatives that have been
  considered during the design, and the resulting design choices. We arrive at a
  minimal design for public transparency logs and associated auditing and
  monitoring functions. The Sigsum log directly provides key usage transparency.
  This can be used as a building block for other transparent systems, and we
  very briefly sketch some of the use cases.

\end{abstract}

\section{Introduction} \label{sec:introduction}

Transparency logs offer detection of events that occur in a system.  Certificate
Transparency detects the unwanted event of a mis-issued certificate~\cite{rfc6962}.
Go's checksum database detects the unwanted event of a malicious Go
module~\cite{sumdb}.  Flexible anonymous networks detect the unwanted event of
bytecode that should not have been pushed to a programmable node~\cite{fans}.
The list of transparency log use cases is extensive and does not end here.  Yet,
many of the basic questions like what to log, where to log it, how to add
enforcement of logging in user software, and who is able to contribute with
reliable transparency log operations remain somewhat open for the long tail of
potential.  Where should one start?

One way to go about the design of transparent systems is to ensure there is a
\emph{building block} that applies to more than a single use case. For example,
much like a VPN provider does not need to know all internal details of the
WireGuard protocol to manage tunnels, it should not be necessary for
them to know all the internal details of a log system to start producing a
transparent relay list.  An up-and-running system of reliable and well designed
transparency logs that cater to the wide variety of potential use cases would go
a long way.

This document introduces a public transparency-log design named \emph{Sigsum}.
It is a minimal building block that allows publishing of signed checksums.  This
ensures detection of two unwanted events: malicious and unintended
\emph{key usage}.  The ability to say with confidence that a signing key has
been used is further useful for layering additional transparency use cases
on top of Sigsum.  In other words, the essential part of a transparency log is
not the exact bits that are in the log, but rather the fact that an event
happened and a monitor that takes interest in that event can go and inspect it
further~\cite{mozilla-bintrans}.  For example, if a new version of a VPN relay
list is signed transparently, then a monitor can detect it in the log,
crosscheck that it is reachable on the service's API, and ensure it is
consistent with the claims made about how it is composed to keep users safe.

For security, Sigsum relies on m-of-n parties to be benign.  These parties are
trusted as a collective and called \emph{witnesses}~\cite{cosi-protocol}.  Each
use case is free to pick and choose which witnesses they want to depend on.
Trust has no one-size-fits-all.  Sigsum also relies on each use case running its own
monitors, much like it is assumed the owners of websites make use of Certificate
Transparency monitors.

Section~\ref{sec:challenges} introduces some of the design space that Sigsum
  aims to cover.
Section~\ref{sec:design} describes the Sigsum system.
%% Section~\ref{sec:further-work} concludes with a few remarks on further work.
The Sigsum log server protocol, its free and open source software
implementation, and the associated tooling can be located online at
\url{https://www.sigsum.org/}.

We assume that the reader is familiar with transparency log primitives like
Merkle trees and digital signatures.

\section{On the design of transparency logs} \label{sec:challenges}

A public transparency log service is a utility that any individual, project, and
organization can rely on to hold themselves or others accountable for claims
they make. For example, consider a free and open source software project that
wants to claim there are no secret backdoors in their releases. A public
transparency log service helps achieve this goal in theory. In practice,
standing up a log, operating it reliably, and avoiding centralized trust in a
single party is quite hard. This section recaps some of the design space that
was kept in mind while building Sigsum. Therefore, it is tailored for logs that
accept log entries from anyone.

\subsection{Purpose of logging}

A good start is to figure out what the purpose of the log is.  This may sound
trivial, but it is one thing to stand up a log and a different thing to make use
of it in a meaningful way.  This will include threat modeling as well as
spelling out what events to detect and which claims are associated with those
events.  Failure to define what events one is aiming to detect or which claims
a monitor should try to falsify may indicate that an append-only log is not the
right tool.

A helpful reference to think about falsifiable claims is the claimant
model~\cite{claimant-model}. Sketching the required verification is further a
helpful exercise to guide log design.

\subsection{Log poisoning} \label{sec:log-poisoning}

Publishing arbitrary data provided by an untrusted user opens up for
\emph{log poisoning}. Due to the strict append-only property of a transparency
log, inappropriate data that makes it into the log cannot easily be removed.
This can become an existential threat for log operators, who may be pressured to
shut down their log or be deterred from starting such operations to begin with.

To see this, consider what happens in case a user submits a data item that it is
illegal to publish.  That data item then gets published by an append-only log,
placing the log operator in a difficult position: if the operator keeps
publishing the item as part of the log, the operator could face legal action. If
the operator deletes the item, that instead violates the log's append-only
property.  A log that violates its (cryptographic) append-only property may be
rejected due to misbehavior by those that rely on it.  This potentially damages
the operator's reputation as well.  The operator's most reasonable option might
be to take the entire log offline, thereby resulting in an indefinite
denial of service attack.

Hence, for a public log server, it is of particular importance to carefully
consider what data should and should not be included in the published log.

\subsection{Log spam} \label{sec:log-spam}

A transparency log requires permanent storage of the logged data.  Excessive
submissions to the log will not only consume networking and computational
resources temporarily, but it also implies consumption of storage resources that
cannot be reclaimed for the lifetime of the log. A log operator may need to
enforce rate limits to bound the daily growth of stored log data. However, a
global limit could let a malicious user consume almost all of that daily storage
quota and starve other users. Having a plan to counter \emph{log spam} while
achieving an acceptable level of fairness is important to operate a reliable
log service.  That plan ideally includes how to also shut down the log without
causing breakage.

In Certificate Transparency, one countermeasure to log spam is to only accept
certificates signed by trusted certificate authorities.  They
help with rate limiting because they typically charge users
before issuing a new certificate, or they impose limits for
individual domain names that require resources to be acquired~\cite{le-rate-limits}.
Another technique that can be used to deal with log spam is sharding, meaning
the log is defined with a scope (like a temporal validity period) so that it can
eventually be turned off to reclaim storage~\cite{temporal-sharding}.

\subsection{Data flows}

%% An intro sentence to set context.

Applications already distribute data using a plethora of different mechanisms
and protocols. A transparency log may include a data distribution service, but
it does not have to. Applications can keep using their existing distribution
mechanisms, and let some of the information needed for additional transparency
piggy back on that.  This will make the public transparency log service lighter.
It will also make the difference between an application's \emph{before and
after} smaller. %% Rephrase

The introduction of a log necessarily changes the data flows of the system.  The new
data flows may expose unintended information to the log or those that monitor
it.  For example, consider the example of a log server that accepts an image and
publishes a checksum of it.  Even if a monitor is unable to download all images
from the log, the log operator would be in possession of all images.
Pay close attention to which information will become public for everyone, and
whether any additional information becomes available only for the log to see.

Data flows are not limited to the bits sent between different parties.  An
easy to overlook change is how end user software should verify that a data
object is present in the log.  Even if that verification is done offline and thus not
requiring any additional outbound network connections, \emph{something} must
be changed in end user software to require transparency logging.  Realizing that the
verifying code needs to change helps, because it influences which formats and
APIs to center the log around. %% Strike or rephrase last sentence.

New data flows risk eroding a user's privacy.  We discuss this
further with regard to log auditing (Section~\ref{sec:auditing}) and split-views
(Section~\ref{sec:split-view}).

\subsection{Auditing} \label{sec:auditing}

A transparency log that is audited properly turns the log operator into a
trusted third party.  To realize the common transparency log notion of
\emph{trust but verify}, proper auditing mechanisms ought to be defined and put
into production.

Auditing means that the operation of a log is independently verified: the log is
challenged to cryptographically prove inclusion and consistency. Log clients
that fetch such proofs can be linked to particular log entries and other tree
head state that they store. Such linking sometimes result in privacy issues. For
example, a web browser that fetches inclusion proofs from a Certificate
Transparency (CT) log trivially reveals the user's browsing history.
Considering the amount of effort Google Chrome spent to get any form of
reasonable auditing in place~\cite{opt-out-sct-auditing}, it is likely a good
idea for new logs to avoid hand waving this aspect.

\subsection{Monitoring} \label{sec:monitoring}

Besides auditing of a log's behavior (like detecting if the append-only property
is being violated), mechanisms are needed to \emph{monitor the contents} of the
log.

A monitor downloads all published entries.  The goal is to detect unwanted
events, and to try to falsify claims related to these events.  For example, the
job of a Certificate Transparency monitor is to detect certificates not
requested by the legitimate website owner.  The goal of a reproducible builds
monitor would be to detect that a new build is available and verify the claim by
rebuilding.

Note how the core value proposition that a transparency log brings is that
end users and monitors see the same events that can be investigated further.  In
other words, it would be pointless to find different certificates or
reproducible builds than the ones encountered by real users.  Regardless of the
transparency log design in question, consider monitoring an application specific
activity.

\subsection{Split-views} \label{sec:split-view}

Two parties that query a log expect to see exactly the same entries,
except that it may be the case that one party sees a slightly older
version of the log, identical except for a few missing entries at the
end. These two views are said to be consistent. The objective of a
\emph{split-view} attack is to provide the two parties with
inconsistent views. For example, one specific log entry could differ
so that Alice sees an authentic software package while Bob sees a
doctored one including malicious code. In other words, a malicious
log can try to maintain multiple versions of its log content and
selectively show them to different users depending on who's asking. If
a split-view attacks goes unnoticed, the purpose of having a log is
defeated.

One approach for detecting split-view attacks is gossip. The participants of a
gossip protocol may be ordinary clients relying on the log, or specialized nodes that
audit the log without themselves relying on it. Gossip protocols exchange
information about each participant's view of the log in hope of detecting
any inconsistencies.  No gossip protocols have been deployed for logs in
practice.  Design of gossip protocols typically need to take privacy
into account~\cite{nordberg}. All \emph{reactive} auditing and gossip will also
need a plan for incident reporting: what happens when a log fails to produce
valid proofs?  That alone only detects \emph{potential} log misbehavior.

%% Rephrase final sentences for clarity; to not read like a critique of
%% detection in general.


A different approach is witness cosigning~\cite{cosi-protocol}. A
\emph{cosignature} is a digital signature on a log's tree head, made by a
different party than the log itself.  A witness cosigns a log's published tree
head, only after verifying that the tree head is consistent with previous tree
heads cosigned by the witness.  The log's own signature and the witnesses'
cosignatures are attached to the published tree head to convince users that they
see the same log entries as the collective of witnesses.  Unlike reactive approaches,
verification for users become binary.  It is up to monitors to manage issues
like witnesses that no longer produce cosignatures.

\section{The Sigsum design} \label{sec:design}

%% @rgdd System figure would go here. It also seems important to
%% stress offline verification.

Sigsum aims to provide key usage transparency. By this, we mean the ability to
detect key misuse, i.e., that something was signed that should not have been
signed. Misuse could happen, for example, due to key compromise, or someone with
legitimate key access being coerced to sign malicious data.  This scoping may
seem narrow.  However, being certain about whether a signing key has been
used is a powerful building block for transparency in general.  One can view it
as setting the record straight with regard to events that occurred in a system.

%% Consider moving TPM example elsewhere

As one example, consider remote attestation.  A software client wants
to ensure that the server it connects to is running the expected
software, before sending any user data to that server. A Trusted
Platform Module (TPM) can be used to provide
this assurance. As the system is booted, a hash of each layer of code
is registered in the TPM's Platform Configuration Registers (PCR). 
The TPM includes a unique secret signing key. At connection
time, the server can ask the TPM to provide a \emph{quote}.  A quote is a digital
signature that covers the values of the PCR registers and the session
key for the communication channel. If the client knows the TPM's
public key and the PCR values that \emph{should be registered} by the TPM in the running
server, the quote gives the client assurance that (i) it is
talking to the right server, and (ii) the server is running the
intended software (provided the booted software is sufficiently locked down,
e.g., without remote shell access to any of the administrators).

For this to work, the server operator has to distribute a public key and
PCR values for all of its servers to the clients.  Here is where a transparency
log becomes useful: are all the clients receiving the same good PCR values, e.g.,
as they are downloaded in signed format from a website?  To answer this question
and back up a claim, these PCR values can be signed and logged with Sigsum.  A
monitor is then able to see for themselves what states users will accept by
inspecting the log and ensuring that matches what is listed on the website.

This section details the Sigsum system and some of the design choices made.

\subsection{Sigsum entities}

%% Structure: Sisgsum "system" == log service + users 
The Sigsum system consists of several entities cooperating to provide a
transparent and audited log service: the \emph{log} server itself,
\emph{witnesses}, and \emph{monitors}. \emph{Submitters} submit entries to be
included in the log. On the receiving end we have \emph{verifiers}, that want to
ensure something is properly logged in the same view that monitors are able to
observe.  Below is an overview of these entities.

\begin{description}
\item[Log] The log server is the main point of contact for log
  submitters. A log server is identified by its public key, and the
  URL where it responds to Sigsum log protocol requests. It maintains a
  reliable storage of the append-only sequence of log entries and a
  corresponding Merkle tree. Each log entry is a signed
  checksum, signed by its submitter. The log periodically signs its
  current tree head, and collects cosignatures for that from witnesses. The tree
  head together with log signature and witness cosignatures is published
  to log users.

\item[Witness] A witness observes tree heads from a log,
  checking that they are consistent. More concretely, a witness'
  cosignature on a tree head claims that the log still includes
  everything that was previously cosigned. Cosignatures also include a
  timestamp.  One of several uses of this timestamp is to detect if a log publishes old
  cosignatures, i.e., it helps to achieve freshness.

\item[Submitter] A submitter uses its submission key to sign a short message
  which is submitted to a log server.
  Usually, this message is the hash of some data item the submitter intends to
  distribute. It also queries the log for a cosigned tree head and an inclusion
  proof that ties the submitted message to that tree head. The result is then
  packaged as a proof of logging. The proof can be handled and distributed in a
  way similar to handling a detached signature on the data item.

\item[Verifier] A verifier receives a data item together with a proof of
  logging, typically using the same distribution mechanism already in place for
  the data item. It verifies, offline, that the proof is valid according to
  policy. The verifier's policy defines known logs, trusted witnesses, and the
  details of how many cosignatures are required for the data to be accepted.
  
\item[Monitor] A monitor periodically requests the latest tree head
  and corresponding leaves from each of the logs being monitored. A monitor can
  detect log misbehavior, witnesses that seem to be absent, and unexpected log
  entries.

  %% Overhaul
  A monitor usually takes particular interest in certain submission
  keys. It filters out all leaves signed by one of the keys of
  interest. The key owner is required to define the policies and
  procedures needed compare these logged signatures to expectations,
  and take action on unexpected or unauthorized signatures.

  For use cases where signatures by a submission key of interest
  implies additional claims about the signed data, a monitor could
  obtain the data out of band and verify those claims. For example, if a
  signature claims that an executable with a certain hash was built
  from a certain source package, the monitor could trigger a rebuild
  of the package to verify it.
\end{description}

\subsection{Threat model}

%% Overhaul

%% Move elsewhere.
By key usage transparency we mean the ability of a submitter, or others relying
on their submission key, to detect unexpected signatures. When using Sigsum,
traditional signing of data items is augmented to produce a proof of logging.
The verifier is assumed to have an authentic copy of the submitter's public key,
and a trust policy.

The security objective of the Sigsum system is as follows: if an unauthorized
signature is made by a submission key, then the signature will either be (i)
refused by the verifier, or (ii) detected after the fact by a monitor that takes
an interest in that subission key, or (iii) log monitoring will raise alarms of
potential log misbehavior (e.g., indicated by not observing enough recent
cosignatures).

%% Second part of this is a security claim, not the threat model
The threat model includes compromise of the submitter's private key and
distribution system, compromise of the log itself, and compromise of some (but
not too many, subject to configured trust policy) of the witnesses. In this
setting, the attacker can sign a data item and produce a valid proof of logging
accepted by the verifier. However, as long as a sufficient number of witnesses
are \emph{not} compromised, the unauthorized signature stays in the public
record as long as the log server is still up and running.  Note that detection
of a log that refuses to provide services or is otherwise unreachable due to, e.g., a
denial of service attack is trivially detected.  For an attack to be in scope of
the Sigsum system, the attacker needs to find a way to stay under the radar and
not be detected.

\subsection{Data stored in the log}

A submission to a Sigsum log consists of three short data items: A
fixed size message, an Ed25519 public key, and a signature over the
hash of the provided message. We refer to the hash of this message as
the \emph{checksum}. Usually, the the message itself is the hash of a
data item to be distributed out of band by the submitter.

The log verifies the signature, and if it is valid, the submission is
transformed into a log entry that consists of the hash of the message,
i.e., the checksum, the hash of the submitter's public key, and
the signature. This is illustrated in Figure~\ref{fig:data}. The log
entry is inserted as a \emph{leaf} into the Merkle tree and published
by the log. The total leaf size is a mere 128 octets.

\begin{figure}[h]
  \centerline{\begin{tabular}[t]{l|l|l}
    Log request & Log entry & Octet size\\
    \hline 
    message & checksum $= \text{SHA256}(\text{message})$ & 32\\
    public key & key hash $= \text{SHA256}(\text{public key})$ & 32\\
    signature &  signature & 64 \\
  \end{tabular}}
\caption{A log request and the corresponding log entry published by the
  log}\label{fig:data}
\end{figure}

%% @rgdd: Would be nice with a figure to illustrate both the
%% submission fields and the resulting leaf. Alternatively, soem LaTeX
%% tables.

Note that none of the published bits is under immediate control of the
submitter. If the submitter wants the log to publish some particular
chosen bits, the submitter would have to try various messages, private
keys, or (non standard) signature nonces, at an exponential cost in
the number of bits controlled. This makes log poisoning a lot harder,
and helps to keep the log operator not liable for publishing hashes of
arbitrary submitted data.

\subsection{Rate limiting and log spam}

Sigsum does not specify any measures to deal with denial of service
and log server overload in general. But the protocol includes one
optional mechanism to rate limit the number of added leaves, based on
submitter's domain name.

To use this mechanism, the submitter registers a public key as a TXT
record on a DNS name they control, and the corresponding private key
is used to create a submit token. The domain and the token are
attached to submit requests using a custom HTTP header.
The log server looks up the corresponding public key in DNS, and
attempts to verify the token, to validate that the submitter is
associated with that domain. The log server can then apply per domain
rate limits. For example, a daily quota per registered
domain can be used~\cite{public-suffix}.

The generated submit token is bound to the log's identity via its public key.
This ensures that the log can't use the token to submit any data to other logs.

\subsection{Witnesses cosigning, no gossip}

Sigsum does not use gossip. In many use cases such as software
updates, such reactive verification provides little or no protection: by the
time a rogue software update is installed, the attacker can disable
the gossip mechanism, or modify the local state maintained by the
gossip protocol, to make everything look consistent. Gossip protocols
also implies additional communication, with application specific
privacy implications. This makes it very difficult to design a gossip
protocol for an application agnostic transparency log like Sigsum.

Sigsum instead uses proactive witness cosigning. This idea was originally
proposed by Syta~\emph{et~al.}~\cite{cosi-protocol}. Their protocol was designed
to provide both low latency and scalability to thousands of cosigning witnesses.
We value witness quality over quantity, and expect most transparency logs to
have no more than a dozen of witnesses or so in practise because there is a
diminishing return in diversity for each witness that is added.  We are also
willing to accept a little bit of latency, as well as non optimized
communication overhead on the wire.  Therefore, Sigsum cosignatures use plain
Ed25519 as opposed to threshold signatures.  Communication is directly between a
log and each of its witnesses.  Each witness operates independently, with no
interaction with other witnesses.

A witness' cosignature is a digital signature that covers
(i) a log's identity, represented by a hash of the log's public key,
(ii) current time, and (iii) a tree head (i.e., root hash and size)
published by the log. The witness' cosignature makes the claim that,
by the given time, the tree head is the largest one observed by the
witness, \emph{and} that the tree is consistent with (and no smaller
than) all other tree heads that the witness have ever cosigned for
that log.  Note that inclusion of a timestamp in the cosignature helps
convince monitors of freshness.

Verifiers are expected to require cosignatures by a sufficient number
of known witnesses. Monitors are expected to raise alarms if recent
cosignatures are missing on a log. This setup ensures that verifiers and
monitors have the same view of the log, even if an attacker controls
the log and some (but not too many) of the witnesses.

\subsection{Witness interoperability}

%% When revising, I think it's a nice point that the narrow scope of
%% witnesses, that they only care about a log's internal consistency,
%% is what makes it possible and even easy for a witness to operate
%% with logs serving many different applications.

A useful witness is reputable, and operated with high security and reliability.
Beyond public and general purpose logs like Sigsum, there are other flavors of
transparency logs that could benefit from witness cosigning. The ability to
interoperate with other log systems and depend on each other's witnesses will be
a long term win for everyone.  The pursuit of such interoperability spans both
the wire protocol and APIs between logs and witnesses, as well as the
lower level cryptography like Merkle tree definitions and which octets are
cosigned.  What is cosigned in Sigsum has already been described.  Like all
other current transparency logs, Sigsum models its Merkle tree and proofs based
on RFC~6962~\cite{rfc6962}.

The exact protocol between logs and witnesses is under active development and
will be described further in a future revision of this or another document.

\subsection{Monitoring}

A witness' job is limited to checking consistency of its
view of a log, without any interest in neither the logged data nor in
what any other witness might be doing. This limited scope is helpful
to make witnesses easy to operate securely.

Monitoring is a separate responsibility. Monitors retrieve a log's
published tree heads and check consistency, similarly to a witness.
But in contrast, a monitor has an interest in both the corresponding
log entries \emph{and} the witnesses' cosignatures to be sure there
are no split-views.  Any missing cosignature may indicate that the
witness in question is observing, and cosigning, a log with different
entries.  Monitors are expected to initiate investigation of such
issues.

\subsection{Choice of cryptographic primitives}

Sigsum specifies a fix concrete choice of cryptographic primitives,
namely SHA256 as the hash function and Ed25519 as the digital
signature algorithm\footnote{Ed25519 internally uses SHA512, but that
  is not visible elsewhere in the Sigsum protocols}. All signatures use
fixed namespace prefixes on the signed data for domain separation.

When there's a need to revise these choices, for example, to add
support for algorithms that are resistant to attackers with quantum
computers, that would be a new incompatible revision of the Sigsum
protocols. Any negotiation (automatic or otherwise) between Sigsum
parties will be on protocol version, not on individual primitives or
cipher suites.

\subsection{Log server protocol}

The Sigsum Log protocol, i.e., the publicly advertised interface of a
Sigsum log server, is a REST-like HTTP API. The data exchanged in
requests and responses does not need much structure, and is
represented using a simple ASCII key-value format. The complete list or
protocol requests are: \texttt{get-tree-head}, \texttt{get-inclusion-proof},
\texttt{get-consistency-proof}, \texttt{get-leaves}, and \texttt{add-leaf}.

%% FIXME: Cite v1 log spec.

%% \section{Further work} \label{sec:further-work}

\subsection*{Acknowledgments}

Mostly written by Niels Möller and Rasmus Dahlberg from the Sigsum project.

\bibliographystyle{plain}
\bibliography{refs}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

% LocalWords:  sigsum cryptographic Merkle cosignature mis bytecode
% LocalWords:  cosignatures serverless checksums verifier submitter's
% LocalWords:  submitters verifiers verifier's namespace WireGuard
% LocalWords:  nonces reproducibly sigstore backend backdoors SCTs
% LocalWords:  rekor reproducibility CoSi Sigsum's FIXME untrusted
% LocalWords:  sharding cryptographically Niels Möller Rasmus
% LocalWords:  Dahlberg
