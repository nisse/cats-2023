\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{nopageno}
\usepackage{hyperref}
\usepackage{url}

\def\UrlBreaks{\do\/\do-}

\usepackage{color}

\definecolor{darkblue}{RGB}{0,0,139}
\hypersetup{
  colorlinks,
  citecolor=darkblue,
  linkcolor=darkblue,
  urlcolor=darkblue,
}

\title{%
  \LARGE{A Minimalistic and Public Transparency Log}\\[.2cm]
  \Large{Sigsum's Design and Available Tooling}%
}
\author{Niels Möller}
\date{2023-11-30}

\begin{document}

\maketitle

\section{Introduction}

Sigsum---short for \emph{sig}ned check\emph{sum}---is a minimalistic
transparency log design that offers key-usage transparency.  This design is the
result of the free and open-source software project with the same
name~\cite{sigsum-project}.  A Sigsum log can accept signed checksum
submissions for a wide variety of applications and entities that are
neither known, nor trusted, by the log operator.  One of the many
use-cases of Sigsum logging is transparency for signed software packages
and updates.

In this talk, we describe the design at large and how the recently released
version-1 system can be used~\cite{log-spec}.  A few live demos of the
different system components are included, such as using our tooling to interact
with a log.

\section{Design considerations}

Sigsum's public log design is simple and minimalistic; a building block
that a variety of applications can layer on-top (or take inspiration
from) to detect unwanted system events.  Simplicity ranges from not
supporting cryptographic agility to excluding so-called SCTs and
hand-waving of gossip-like protocols.  Instead, users verify offline
proofs that can be tied back to a witness cosigning protocol that offers
security if a quorum of witnesses are benign.  Examples of minimalism
include what is stored in a log's Merkle tree leaves (only a checksum of
some data distributed out-of-band, a signature, and the submitter's key
hash) as well as the required parsers to work with our formats (no
X.509, ASN.1, etc).

As a brief summary, we carefully considered the purpose of logging,
what is logged, auditing, gossip, anti-poison, anti-spam, privacy,
APIs, and ease-of-use for log operators as well as
users~\cite{design-considerations}. The RFC 6962 hashing strategy is
used for the log's Merkle tree. Tree heads and witness cosigning are
cryptographically compatible with Go's checksum database and the
Omniwitness. Ed25519 and SHA256 are used as our cryptographic
primitives.

\section{Specifications}

The main Sigsum interfaces and artifacts are the network protocol used
to interact with log servers, the contents of a proof of logging, and a
trust policy defining known logs and witnesses as well as how much
cosigning is required.

\subsection{Protocol}

Sigsum log servers implement an \textsc{http rest} interface with text
representations of inputs and outputs~\cite{log-spec}. The available
\textsc{get} operations are:
\texttt{get-tree-head}, \texttt{get-inclusion-proof},
\texttt{get-consistency-proof}, \texttt{get-leaves}.  There is a
single \textsc{post} operation: \texttt{add-leaf}.
An \texttt{add-leaf} request may be subject to domain-based rate
limiting.  The submitter must then include an \textsc{http} header with
a token tied to a public key registered as a \textsc{txt} record on
the submitter's domain.

Interaction between logs and witnesses uses a separate witness
protocol~\cite{witness-spec}.

\subsection{Sigsum proof}

A Sigsum proof identifies a particular Sigsum log (by the hash of the
log's public key), a leaf in that log, a cosigned tree head, and an
inclusion proof for the leaf. While applications may use their own
serialization for this, there is a specification~\cite{sigsum-proof}
that defines the contents of the proof, how to verify it, and the text
format used by the Sigsum command line tools.

\subsection{Sigsum policy}

A Sigsum policy file~\cite{sigsum-policy} lists the public keys, and optionally
\textsc{url}s, of known logs and witnesses. It also defines the
\emph{quorum}, i.e., the rule that determines whether or not a subset
of witnesses that have cosigned a tree head is strong enough to
consider the tree head to be valid. E.g., the quorum can require valid
cosignature from at least $k$ out of $n$ trusted witnesses.

\section{Using the Sigsum system}

To start using the Sigsum system, one can use a public log, e.g.,
operated by the Sigsum project, or set up one's own. One also needs a
set of trusted witnesses. The choices of logs and witnesses are the
basis of the Sigsum policy, and it is essential that verifiers and one's
monitor rely on the same log and witnesses.

Next, one needs to setup a monitor following the log, configured with
the keys for which one wants transparency. Procedures for acting on
alerts from the monitor must be defined.

With this setup, distribution of software packages would be extended
to submit signatures to the log and collect a corresponding Sigsum
proof to distribute along with the package. Receivers of these
packages should then be configured to require a proof of logging, in
addition to other validity checks, according to the Sigsum policy. The
Sigsum command line tools~\cite{sigsum-tools} make both submit and
verify operations easy.

\section*{Speaker}

Niels Möller is a software developer, maintainer of the \textsc{gnu}
Nettle crypto library for the last few decades, and working in the
Sigsum project during the last year. The Sigsum project is funded by
Glaskar Teknik AB, a sister company of Mullvad \textsc{vpn}. Niels
will be attending the workshop in person.

\bibliographystyle{plain}
\bibliography{refs}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
